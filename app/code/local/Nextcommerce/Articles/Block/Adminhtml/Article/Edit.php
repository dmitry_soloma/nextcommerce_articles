<?php
/**
  * Adminhtml Article Edit
  *
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
  * @copyright   Copyright (c) 2015 Nextcommerce
  * @author      dmitry.soloma
*/
 
/**
  * Nextcommerce_Articles_Block_Adminhtml_Article_Edit
  * 
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
*/
class Nextcommerce_Articles_Block_Adminhtml_Article_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct(); 
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_article';
        $this->_blockGroup = 'nextcommerce_articles';
        $this->_headerText = $this->__('Edit Article');
        $this->_updateButton('save', 'label', Mage::helper('nextcommerce_articles')->__('Save'));
        
        $this->_addButton(
            'save_and_continue', 
            array(
                'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class' => 'save'
            ),
            -100
        );
        
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }";
    }
}