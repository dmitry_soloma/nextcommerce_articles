<?php
/**
  * Adminhtml Article Edit Form General Tab
  *
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
  * @copyright   Copyright (c) 2015 Nextcommerce
  * @author      dmitry.soloma
*/
 
/**
  * Nextcommerce_Articles_Block_Adminhtml_Article_Edit_Tag_General
  * 
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
*/
class Nextcommerce_Articles_Block_Adminhtml_Article_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $helper = Mage::helper('nextcommerce_articles');
        $id = $this->getRequest()->getParam('id');
        $this->setForm($form);
        $fieldset = $form->addFieldset('nextcommerce_articles_form_fieldset', 
            array('legend' => $helper->__('Article Fields'))
        );
        
        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'name'      => 'status',
            'values' => array(
                '1' => 'Enabled',
                '0' => 'Disabled'
            )
        ));

        $fieldset->addField('name', 'text', array(
            'label' => $helper->__('Article Name'),
            'name' => 'name',
            'class' => '',
            'required' => true,
            'values' => '',
            'disabled' => false,
            'note' => $helper->__('Enter Article Name')
        ));
        
        $fieldset->addField('article_content', 'editor', array(
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            'wysiwyg'   => true,
            'name' => 'content',
            'label' => $helper->__('Article Content'),
            'title' => $helper->__('Article Content'),
            'style' => 'width:700px; height:300px;'
        ));
        
        $fieldset->addField('file_name', 'image', array(
            'label' => $helper->__('Image'),
            'name' => 'file_name',
            'class' => '',
            'required' => false,
            'values' => '',
            'disabled' => false,
            'readonly' => false,
            'note' => $helper->__('Upload Image')
        ));
        
        if ($id){
            $data = $this->getDataFromModel($id);
            $form->addValues($data);
        }
        
        return parent::_prepareForm();
    }
    
    /**
     * Get array data from model
     * 
     * @param int $id
     * @return array
     */
    public function getDataFromModel($id)
    {
        $record = Mage::getModel('nextcommerce_articles/article')->load($id);
        $record->setArticleContent($record->getContent());
        
        return $record->getData();
    }
}