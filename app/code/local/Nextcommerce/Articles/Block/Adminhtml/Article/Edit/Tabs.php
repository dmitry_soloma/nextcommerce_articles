<?php
/**
  * Adminhtml Article Tabs
  *
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
  * @copyright   Copyright (c) 2015 Nextcommerce
  * @author      dmitry.soloma
*/
 
/**
  * Nextcommerce_Articles_Block_Adminhtml_Article_Edit_Tabs
  * 
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
*/
class Nextcommerce_Articles_Block_Adminhtml_Article_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('nextcommerce_articles_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('nextcommerce_articles')->__('Edit Article'));
    }
 
    protected function _beforeToHtml()
    {
        $this->addTab('nextcommerce_articles_tab_general', array(
            'label'     => Mage::helper('nextcommerce_articles')->__('General'),
            'title'     => Mage::helper('nextcommerce_articles')->__('General Tab'),
            'content'   => $this->getLayout()->createBlock('nextcommerce_articles/adminhtml_article_edit_tab_general')->toHtml()
        ));

        return parent::_beforeToHtml();
    }
}