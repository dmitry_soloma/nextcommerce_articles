<?php
/**
  * Adminhtml Article Grid
  *
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
  * @copyright   Copyright (c) 2015 Nextcommerce
  * @author      dmitry.soloma
*/
 
/**
  * Nextcommerce_Articles_Block_Adminhtml_Article_Grid
  * 
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
*/
class Nextcommerce_Articles_Block_Adminhtml_Article_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function _construct() 
    {
        parent::_construct();
        $this->setId('nextcommerce_articles_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare Collection
     * 
     * @return Nextcommerce_Articles_Block_Adminhtml_Article_Grid
     */
    protected function _prepareCollection() 
    {
        $collection = Mage::getModel('nextcommerce_articles/article')->getCollection();
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }
    
    /**
     * Prepare columns
     * 
     * @return Nextcommerce_Articles_Block_Adminhtml_Article_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
                'header'    => $this->__('Article Id'),
                'align'     =>'left',
                'type'      => 'number',
                'index'     => 'id',
                'width'     => '10%'
        ));
        
        $this->addColumn('name', array(
                'header'    => $this->__('Article Name'),
                'align'     =>'left',
                'index'     => 'name',
                'width'     => '80%'
        ));
        
        $this->addColumn('status', array(
                'header'    => $this->__('Status'),
                'align'     => 'left',
                'index'     => 'status',
                'type'      => 'options',
                'options'   => array (
                    1 => 'Enabled',
                    0 => 'Disabled'
                ),
                'width'     => '10%',
                'frame_callback' => array($this, 'decorateStatus')
        ));
        
        $this->addColumn('action',
            array(
                'header'    =>  $this->__('Action'),
                'width'     => '5%',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => $this->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'is_system' => true
        ));

        return parent::_prepareColumns();
    }
    
    /**
     * Decorate status column values
     *
     * @param string $value
     * @param Nextcommerce_Articles_Model_Article $row
     *
     * @return string
     */
    public function decorateStatus($value, $row)
    {
        switch ($row->getStatus()) {
            case Nextcommerce_Articles_Model_Article::STATUS_ENABLED:
                $value = 'Enabled';
                $class = 'grid-severity-notice';
                break;
            case Nextcommerce_Articles_Model_Article::STATUS_DISABLED:
                $class = 'grid-severity-major';
                $value = 'Disabled';
                break;
            default:
                $class = 'grid-severity-critical';
                $value = 'NULL';
                break;
        }
        
        return '<span class="' . $class . '"><span>' . $value . '</span></span>';
    }

    /**
     * get row url
     * 
     * @param mixed $row
     * @return
     */
    public function getRowUrl($row) 
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}