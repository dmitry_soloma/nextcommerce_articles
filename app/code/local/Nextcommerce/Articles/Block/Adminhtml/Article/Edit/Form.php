<?php
/**
  * Adminhtml Article Edit Form
  *
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
  * @copyright   Copyright (c) 2015 Nextcommerce
  * @author      dmitry.soloma
*/
 
/**
  * Nextcommerce_Articles_Block_Adminhtml_Article_Edit_Form
  * 
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
*/
class Nextcommerce_Articles_Block_Adminhtml_Article_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    { 
        parent::_prepareForm();
        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        
        return $this;
    }
}