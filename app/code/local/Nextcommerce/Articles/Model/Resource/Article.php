<?php
/**
  * Article resource model
  *
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
  * @copyright   Copyright (c) 2015 Nextcommerce
  * @author      dmitry.soloma
*/
 
/**
  * Nextcommerce_Articles_Model_Resource_Article
  * 
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
*/
class Nextcommerce_Articles_Model_Resource_Article extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('nextcommerce_articles/article', 'id');
    }
}