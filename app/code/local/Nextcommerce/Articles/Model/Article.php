<?php
/**
  * Article model
  *
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
  * @copyright   Copyright (c) 2015 Nextcommerce
  * @author      dmitry.soloma
*/
 
/**
  * Nextcommerce_Articles_Model_Article
  * 
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
*/
class Nextcommerce_Articles_Model_Article extends Mage_Core_Model_Abstract
{
    const STATUS_DISABLED = '0';
    const STATUS_ENABLED = '1';
    
    public function _construct()
    {
        parent::_construct();
        $this->_init('nextcommerce_articles/article');
    }
}
