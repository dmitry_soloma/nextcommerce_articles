<?php
/**
 * Install Article Table
 *
 * @category    Nextcommerce
 * @package     Nextcommerce_Articles
 * @copyright   Copyright (c) 2015 Nextcommerce
 * @author      dmitry.soloma
 */

$this->startSetup();

    $table = $this->getTable('nextcommerce_articles/article');
    if($this->tableExists($table)) {
        $this->getConnection()->dropTable($table);
    }

    $table = $this->getConnection()->newTable($table);

    $table->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary' => true,
            'unsigned' => true,
            'auto_increment' => true ),
            'Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(), 'Article name')
    ->addColumn('content', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Content')
    ->addColumn('file_name', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Image File Name')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(), 'Enabled')
    ->setComment('Nextcommerce Article Table');

    $this->getConnection()->createTable($table);

$this->endSetup();