<?php
/**
  * Adminhtml Article controller
  *
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
  * @copyright   Copyright (c) 2015 Nextcommerce
  * @author      dmitry.soloma
*/
 
/**
  * Nextcommerce_Articles_Adminhtml_ArticleController
  * 
  * @category    Nextcommerce
  * @package     Nextcommerce_Articles
*/
class Nextcommerce_Articles_Adminhtml_ArticleController extends Mage_Adminhtml_Controller_Action
{
   
    public function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('nextcommerce/articles');
    
        return $this;
    }
    
    public function indexAction()
    { 
        $this->_initAction();
        $this->renderLayout();
    }
    
    public function editAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }
    
    public function newAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }
    
    /**
     * Upload Image and getFilename
     * 
     * @return string
     */
    protected function _getImageFileName()
    {
        $imageFileName = null;
        if(isset($_FILES['file_name']['name']) and (file_exists($_FILES['file_name']['tmp_name']))) {
            try {
                $uploader = new Varien_File_Uploader('file_name');
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = Mage::getBaseDir('media') . DS ;
                $uploader->save($path, $_FILES['file_name']['name']);
                $imageFileName = $_FILES['file_name']['name'];
            } catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('nextcommerce_articles')->__('Image upload failed.')
                );
            }
        }
        
        return $imageFileName;
    }
    
    public function saveAction()
    {
        $id = $this->getRequest()->getParam('id');
        $article = Mage::getModel('nextcommerce_articles/article');
        $imageFileName = $this->_getImageFileName();
        $data = array(
            'name' => $this->getRequest()->getParam('name'),
            'content' => $this->getRequest()->getParam('content'),
            'status' => $this->getRequest()->getParam('status'),
        );
        if ($imageFileName !== null) {
            $data['file_name'] = $imageFileName;
        }
        if ($id) {
            $article = $article->load($id);
            $article->addData($data);
        } else {
            $article->setData($data);
        }
        try {
            $article->save();
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('nextcommerce_articles')->__('Article Successfully Saved')
            );
        } catch (Exception $e){
            Mage::log($e->getMessage());
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('nextcommerce_articles')->__('Article Can not be saved.')
            );
        }
        if ($this->getRequest()->getParam('back')) { 
            $this->_redirect('*/*/edit', array('id' => $id, '_current' => true));
        } else {
            $this->_redirect('*/*/');
        }
    }
    
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $article = Mage::getModel('nextcommerce_articles/article')->load($id);
            if (!$article->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('nextcommerce_articles')->__('Article Model does not exists')
                );
            } else {
                $article->delete();
                 Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('nextcommerce_articles')->__("Article{$id} has been removed.")
                );
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('nextcommerce_articles')->__('Article Id Require')
            );
        }
        
        $this->_redirect('*/*/');
    }
}